package main

import (
	"fmt"

	"gitlab.com/muhammad.mirza3107/gozatime/gozatime"
)

func main() {
	now := gozatime.TimeEpoch()
	human := gozatime.HumanEpoch(now, "DateTime")

	fmt.Println(human)
}
