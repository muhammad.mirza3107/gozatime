package gozatime

import (
	"strconv"
	"time"
)

func TimeEpoch() int64 {
	time := time.Now().Unix()
	return time
}
func HumanEpoch(e int64, t string) string {
	tanggal := time.Unix(e, 0).Day()
	bulan := time.Unix(e, 0).Month()
	tahun := time.Unix(e, 0).Year()
	jam := time.Unix(e, 0).Hour()
	min := time.Unix(e, 0).Minute()
	detik := time.Unix(e, 0).Second()
	switch t {
	case "Date":
		return strconv.Itoa(tanggal) + " - " + bulan.String() + " - " + strconv.Itoa(tahun)
	case "Time":
		return "(" + strconv.Itoa(jam) + ":" + strconv.Itoa(min) + ":" + strconv.Itoa(detik) + ")"
	case "DateTime":
		return strconv.Itoa(tanggal) + " - " + bulan.String() + " - " + strconv.Itoa(tahun) + " (" + strconv.Itoa(jam) + ":" + strconv.Itoa(min) + ":" + strconv.Itoa(detik) + ")"
	default:
		return strconv.Itoa(tanggal) + " - " + bulan.String() + " - " + strconv.Itoa(tahun)
	}
}
